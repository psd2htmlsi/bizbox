const path = require('path')
const mix = require('laravel-mix')

const cssImport = require('postcss-import')
const cssNesting = require('tailwindcss/nesting')
const tailwindcss = require("tailwindcss");
const autoprefixer = require("autoprefixer");



/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath(`./`);

mix
    .postCss('web/assets/resource/css/style.css', 'web/assets/css/', [
        cssNesting, tailwindcss, cssImport, autoprefixer,
    ])
    .options({
        processCssUrls: false,
    })
    .version();





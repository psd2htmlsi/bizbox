jQuery(document).ready(function () {
	//$('.md-cookies').addClass('md-show'); // for testing

	if (!Cookies.get('cookie_agree')) {
		$('.md-cookies').addClass('md-show')
	}

	$('#md-cookies #save-all').click(function (i) {
		$("[name='thirt_part_cookie']").prop('checked', true)
		save()
	})

	$('#md-cookies #save').click(function (i) {
		save()
	})

	function save() {
		Cookies.set('cookie_agree', '1', { expires: 365, path: '/' })

		if ($("[name='thirt_part_cookie']").is(':checked')) {
			Cookies.set('3_part_cookie_agree', '1', { expires: 365, path: '/' })
		} else {
			Cookies.remove('3_part_cookie_agree')
		}
		//$('.md-cookies').removeClass('md-show')
		location.reload()
	}
})

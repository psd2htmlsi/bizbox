jQuery(document).ready(function($) {
    
    var overlay = $('.md-overlay' );

    $('.md-trigger').each(function( index ) {
        var el = $(this);
        hash = el.data( 'modal' ) ? '#' + el.data( 'modal' ) : el.attr('href'); 
        var modal = $(hash); 

        el.click(function(e) {
            e.preventDefault();
            $('.md-modal').removeClass( 'md-show' );
            modal.addClass( 'md-show' );
            $('html').addClass('overflow-hidden h-full');
        });

        overlay.click(function(e) {
            e.preventDefault();
            closeModal(modal);
        });
    });

    $('.md-close').click(function(e) {
        e.preventDefault();
        modal = $(this).closest('.md-modal');
        closeModal(modal);
    });

    function closeModal(modal) {
        modal.removeClass( 'md-show' );
        $('html').removeClass('overflow-hidden h-full');
    }
});

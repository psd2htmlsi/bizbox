jQuery(document).ready(function () {
	$('.toggleFilter').click(function (i) {
		var toggleButton = $(this)
		$('#filter').toggleClass('hidden')

		$('[data-open]').toggleClass('hidden')
		$('[data-close]').toggleClass('hidden')
	})
})


jQuery(document).ready(function($) {

    var allMods = $(".animation");
    
    window.onscroll = function() {
        scroll()
    };
    
    function scroll() {
        allMods.each(function(i, el) {
			var el = $(el);
			if (el.visible(true)) {
				el.addClass("start"); 
			} 
        });
    }
    scroll();
});
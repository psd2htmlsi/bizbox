jQuery(document).ready(function () {
	//open mega menu
	$('.menu-toggler').click(function (i) {
		$(this).toggleClass('navbar-nav-toggle-active')
		$('.menu-toggler i').toggleClass('icon-menu icon-close').toggleClass('text-dark-500 text-blue')
		$('.menu-collapse').toggleClass('hidden')
		$('html').toggleClass('overflow-hidden')
	})

	//open mega menu
	$('.support-btn').click(function (i) {
		$(this).closest('.support-toogler').toggleClass('translate-x-[16rem]')
		$('.support-toogler .open').toggleClass('hidden')
		$('.support-toogler .close').toggleClass('hidden flex')
	})

	//carousel
	$('.carousel').each(function () {
		var $carousel = $(this).flickity()

		var $carouselHolder = $carousel.closest('.carousel-holder')

		var $cellButtonGroup = $('.pagination-dots', $carouselHolder)
		var $cellButtons = $cellButtonGroup.find('button', $carouselHolder)
		var flkty = $carousel.data('flickity')

		$cellButtonGroup.on('click', 'button', function () {
			var index = $(this).index()
			$carousel.flickity('select', index)
		})

		$('.btn-previous', $carouselHolder).on('click', function () {
			$carousel.flickity('previous')
		})

		$('.btn-next', $carouselHolder).on('click', function () {
			$carousel.flickity('next')
		})

		// update selected cellButtons
		$carousel.on('select.flickity', function () {
			$cellButtons.filter('.is-selected').removeClass('is-selected')
			$cellButtons.eq(flkty.selectedIndex).addClass('is-selected')
		})
	})

	$('.faq-item').each(function (index) {
		var faqItem = this

		$('.faq-title', faqItem).on('click', function () {
			$('.faq-title', faqItem).toggleClass('bg-blue-50')
			$('.faq-title i', faqItem).toggleClass('text-dark-500 text-blue').toggleClass('icon-chevron-down icon-chevron-up')
			$('.faq-content', faqItem).toggleClass('hidden')
		})
	})

	$('.faq-question', this).on('click', function () {
		$(this).toggleClass('bg-blue-50 font-bold')
		$(this).next().toggleClass('hidden block')
	})

	$('.counter').each(function () {
		var $this = $(this),
			countTo = $this.attr('data-count')
		$({
			countNum: $this.text(),
		}).animate(
			{
				countNum: countTo,
			},
			{
				duration: 1500,
				easing: 'linear',
				step: function () {
					$this.text(numberWithCommas(Math.floor(this.countNum)))
				},
				complete: function () {
					$this.text(numberWithCommas(this.countNum))
				},
			}
		)
	})
})

function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
}

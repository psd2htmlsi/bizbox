<?php

return [
    '*' => [
        'pluginName' => 'Feed Me',
        'cache' => 60,
        'requestOptions' => [
            'headers' => [
                'Accept' => 'application/json',
            ],
            'auth' => [
                'emporijprod', 'MInOGrAbLuck'
            ],
        ],        
    ]
];
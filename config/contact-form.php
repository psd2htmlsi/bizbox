<?php

use craft\contactform\models\Submission;
use yii\base\Event;

// ...

Event::on(Submission::class, Submission::EVENT_AFTER_VALIDATE, function(Event $e) {
    $submission = $e->sender;

    if(isset($submission->message['form']) && $submission->message['form'] == 'inquiry') {

        if (empty($submission->message['fromTaxId'])) {
            $submission->addError('fromTaxId', 'Preverite vnosno polje.');
        }
        if (empty($submission->message['fromPhone'])) {
            $submission->addError('fromPhone', 'Preverite vnosno polje.');
        }
        if (empty($submission->fromName)) {
            $submission->addError('fromName', 'Preverite vnosno polje.');
        }
        if (empty($submission->fromEmail) || !filter_var($submission->fromEmail, FILTER_VALIDATE_EMAIL)) {
            $submission->addError('fromEmail', 'Preverite vnosno polje.');
        }
       /* if (empty($submission->message['consentTerms'])) {
            $submission->addError('message.consentTerms', 'Preverite vnosno polje.');
        }*/
    }


    if(isset($submission->message['form']) && $submission->message['form'] == 'becomePartner') {

        if (empty($submission->message['fromTaxId'])) {
            $submission->addError('fromTaxId', 'Preverite vnosno polje.');
        }
        if (empty($submission->message['fromPhone'])) {
            $submission->addError('fromPhone', 'Preverite vnosno polje.');
        }
        if (empty($submission->fromName)) {
            $submission->addError('fromName', 'Preverite vnosno polje.');
        }
        if (empty($submission->fromEmail) || !filter_var($submission->fromEmail, FILTER_VALIDATE_EMAIL)) {
            $submission->addError('fromEmail', 'Preverite vnosno polje.');
        }
       /* if (empty($submission->message['consentTerms'])) {
            $submission->addError('message.consentTerms', 'Preverite vnosno polje.');
        }*/
    }
    
});
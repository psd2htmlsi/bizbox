<?php
/**
 * Site URL Rules
 *
 * You can define custom site URL rules here, which Craft will check in addition
 * to any routes you’ve defined in Settings → Routes.
 *
 * See http://www.yiiframework.com/doc-2.0/guide-runtime-routing.html for more
 * info about URL rules.
 *
 * In addition to Yii’s supported syntaxes, Craft supports a shortcut syntax for
 * defining template routes:
 *
 *     'blog/archive/<year:\d{4}>' => ['template' => 'blog/_archive'],
 *
 * That example would match URIs such as `/blog/archive/2012`, and pass the
 * request along to the `blog/_archive` template, providing it a `year` variable
 * set to the value `2012`.
 */

return [
    'podpora/dokumentacija' => ['template' => 'documents'],
    'aktualno/novosti' => ['template' => 'news'],
    'aktualno/novosti-in-objave' => ['template' => 'news'],
    'aktualno/zgodbe-o-uspehu' => ['template' => 'success-stories'],
    'aktualno/baza-znanja' => ['template' => 'knowledge-base'],
    'povprasevanje' => ['template' => 'page/inquiry'],
    'enovice' => ['template' => 'newsletter'],
    'partnerske-resitve/pregled-partnerjev' => ['template' => 'partners'],
    'partnerske-resitve' => ['template' => 'page/partnerSolutions'],
    'partner-solutions' => ['template' => 'page/partnerSolutions'],
    'partner-solutions/partner-review' => ['template' => 'partners'],
    'podpora-za-uporabnike' => ['template' => 'page/supportCenter'],
    'support-center' => ['template' => 'page/supportCenter'],
    'podpora/podpora-za-uporabnike' => ['template' => 'page/supportCenter'],
    'support/customer-support' => ['template' => 'page/supportCenter'],
    'support/documentation' => ['template' => 'documents'],

    'current/news' => ['template' => 'news'],
    'current/news-and-announcements' => ['template' => 'news'],
    'current/knowledge-base' => ['template' => 'knowledge-base'],
    
];
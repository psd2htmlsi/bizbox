
module.exports = {
    content: ["./templates/**/*.{html,twig}"],
    darkMode: false, // or 'media' or 'class'
    theme: {
      container: {
          screens: {
             sm: "100%",
             md: "100%",
             lg: "100%",
             xl: "1316px"
          }
        },
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        white: {
            DEFAULT: '#ffffff',
        },
        orange: {
            DEFAULT: '#F89C33',
        },
        purple: {
            DEFAULT: '#8F5DB7',
        },
        green: {
            DEFAULT: '#A5BD4A',
            500: '#25D366',
        },
        blue: {
          50: '#F7F9FC',
          100: '#E7EDF3',
          200: '#BAC7DC',
          300: '#EAF5FB',
          DEFAULT: '#00B8F1',
        },
        dark: {
            50: '#eff1f3',
          100: '#6E798B28',
          200: '#6E798B29',
          300: '#00000029',
          400: '#999DA2',
          500: '#6E798B',
          600: '#414954',
          700: '#272E39',
          800: '#2E3641',
          900: '#888d95',
          DEFAULT: '#1c253d',
        },
        red: {
            DEFAULT: '#ef4444',
        }
      },
      fontFamily: {
          'body': ['Work Sans', 'Helvetica', 'Arial', 'sans-serif']
      },
      boxShadow: {
          xs: '0px 0px 8px transparent',
          sm: '0 2px 6px 0 rgba(0, 0, 0, 0.15)',
          DEFAULT: '16px 8px 32px #6E798B29',
          none: 'none',
        },
        extend: {
          dropShadow: {
            '3xl': '1px 1px 15px rgba(0, 0, 0, 0.5)',
            '4xl': '1px 1px 15px rgba(0, 0, 0, 0.8)',
          },
          screens: {
            'xs': '420px',
          },
          maxWidth: {
              'container': '1440px',
          },
          backgroundImage: {
            'arrow': "url('/web/assets/images/svg/sm-arrow.svg')",
            'page-hexes': "url('../images/bg/page-hexes.svg')",
            'page-hexes-v2': "url('../images/bg/bg-pattern-2.svg')",
            'split-white-black': "linear-gradient(to bottom, #272e39 68% , white 32%);"
          }
        }
    },
    variants: {
      extend: {
      },
    },
    plugins: [
        require('@tailwindcss/typography'),
        require('@tailwindcss/aspect-ratio'),
        require('@tailwindcss/line-clamp'),
        require('@tailwindcss/forms'),
    ],
  }


  
  
  
  
  